import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    
    #mask to groupby
    mask_mr = df['Name'].str.contains(r'Mr.', na = True)
    mask_ms = df['Name'].str.contains(r'Ms.', na = True)
    mask_miss = df['Name'].str.contains(r'Miss', na = True)
    
    #median values
    x1 = df[mask_mr]['Age'].median()
    x2 = df[mask_ms]['Age'].median()
    x3 = df[mask_miss]['Age'].median()
    
    #count of nan
    y1 = df[mask_mr]['Age'].isna().sum()
    y2 = df[mask_ms]['Age'].isna().sum()
    y3 = df[mask_miss]['Age'].isna().sum()
    
    return [('Mr.', y1, x1), ('Ms.', y2, x2),('Miss', y3, x3)]
